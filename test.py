import mlflow
from custom_model.my_model import AddN


if __name__ == "__main__":
    with mlflow.start_run():    
        mlflow.set_tag('testing', 'true')
        add5_model = AddN(n=17)
        mlflow.pyfunc.log_model('model', code_path=["./custom_model"], python_model=add5_model)
        print('Test script')